<?php

// Define path to basepath directory
defined('BASE_PATH')
|| define('BASE_PATH', realpath(dirname(__FILE__)));

require_once __DIR__ . '/vendor/autoload.php';

$app = new Silex\Application();
$app->register(new Silex\Provider\ServiceControllerServiceProvider());
$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__ . '/app/views',
));

$parser                 = new Symfony\Component\Yaml\Parser();
$settings               = $parser->parse(file_get_contents(BASE_PATH . '/app/config/settings.yml'));
$hostname               = gethostname();
$hostname_settings_path = BASE_PATH . "/app/config/settings.$hostname.yml";

if (file_exists($hostname_settings_path)) {
    $hostname_settings = $parser->parse(file_get_contents($hostname_settings_path));
    $settings          = array_merge($settings, $hostname_settings);
}

$app['settings'] = $settings;

$app['default.controller'] = $app->share(
    function () use ($app) {
        return new controllers\DefaultController($app);
    }
);


$app->get('/', "default.controller:indexAction");