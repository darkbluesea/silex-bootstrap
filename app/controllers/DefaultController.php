<?php

namespace Controllers;

class DefaultController
{
    protected $app;

    public function __construct(&$app)
    {
        $this->app = $app;
    }

    public function indexAction()
    {
        return $this->render();
    }

    public function localAction()
    {
        return $this->render();
    }

    private function render()
    {
        $callers  = debug_backtrace();
        $method   = $callers[1]['function'];
        $template = substr($method, 0, strlen($method) - strlen('Action'));

        return $this->app['twig']->render($template . '.twig');
    }
}
